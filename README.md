# Syslog Watcher

Log viewer for your web server.

With this module you can have access to events logged by the
[Syslog](https://www.drupal.org/documentation/modules/syslog) module.


## Requirements

This module requires no modules outside of Drupal core.

Syslog module has to use a specific file for the Drupal log and this file has
to be readable by the webserver user.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure in Administration » Configuration » Development » Logging and errors:
- Define which file to read.
- If you have changed the separator used on the syslog format, you also have
  to configure it.


## Maintainers

Current maintainers:
- Benjamin Rambaud - [beram](https://www.drupal.org/user/3508624)
- Florent Torregrosa - [Grimreaper](https://www.drupal.org/user/2388214)

Supporting organizations:
- [Smile](https://www.drupal.org/smile)
