<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Url;
use Drupal\syslog_watcher\Services\LineFormatterInterface;
use Drupal\syslog_watcher\Services\LineParserInterface;
use Drupal\syslog_watcher\Services\PagerHelperInterface;
use Drupal\syslog_watcher\SyslogFormatEntry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Syslog Watcher routes.
 */
class SyslogWatcherController extends ControllerBase {

  /**
   * The line formatter.
   *
   * @var \Drupal\syslog_watcher\Services\LineFormatterInterface
   */
  protected $lineFormatter;

  /**
   * The line parser.
   *
   * @var \Drupal\syslog_watcher\Services\LineParserInterface
   */
  protected $lineParser;

  /**
   * The pager helper.
   *
   * @var \Drupal\syslog_watcher\Services\PagerHelperInterface
   */
  protected $pagerHelper;

  /**
   * The pager form.
   *
   * @var \Drupal\Core\Form\FormInterface
   */
  protected $pagerForm;

  /**
   * Constructs a SyslogWatcherController object.
   *
   * @param \Drupal\syslog_watcher\Services\LineFormatterInterface $line_formatter
   *   The line formatter service.
   * @param \Drupal\syslog_watcher\Services\LineParserInterface $line_parser
   *   The line parser service.
   * @param \Drupal\syslog_watcher\Services\PagerHelperInterface $pager_helper
   *   The pager helper service.
   * @param \Drupal\Core\Form\FormInterface $pager_form
   *   The pager form.
   */
  public function __construct(LineFormatterInterface $line_formatter, LineParserInterface $line_parser, PagerHelperInterface $pager_helper, FormInterface $pager_form) {
    $this->lineFormatter = $line_formatter;
    $this->lineParser = $line_parser;
    $this->pagerHelper = $pager_helper;
    $this->pagerForm = $pager_form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('syslog_watcher.line_formatter'),
      $container->get('syslog_watcher.line_parser'),
      $container->get('syslog_watcher.pager_helper'),
      $container->get('syslog_watcher.pager_form')
    );
  }

  /**
   * Displays a listing of syslog log messages.
   *
   * Messages are truncated at 56 chars.
   * Full-length messages can be viewed on the message details page.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function overview() {
    // Initialize variables.
    $rows = [];
    $build = [];
    $header = $this->constructOverviewTableHeader();
    $information_message = [];

    // Syslog module have to use a specific file for the Drupal log.
    $file_path = $this->config('syslog_watcher.settings')
      ->get('syslog_file_path');

    if (empty($file_path)) {
      $syslog_logging_settings_path = new Url('system.logging_settings');
      $this->messenger()
        ->addError($this->t('No file path has been found. <a href=":url">Check the configuration</a>.', [':url' => $syslog_logging_settings_path->toString()]));
      return $build;
    }

    if (\is_string($file_path) && \is_readable($file_path)) {
      $file = new \SplFileObject($file_path);

      // Go to the end of the file to get the number of lines.
      $file->seek(\PHP_INT_MAX - 1);
      $lines_number = $file->key();

      // Initialize the pager.
      [$start_line, $end_line] = $this->pagerHelper->initPager($lines_number);
      if (!isset($start_line) || !isset($end_line)) {
        $this->messenger()
          ->addError($this->t('An error occurred during the pager initialization.'));
        return $build;
      }

      // Add the pager form.
      $build['syslog_watcher_pager_form'] = $this->formBuilder()->getForm($this->pagerForm);

      $information_message = [
        $this->t('You are reading the log file :file_path.', [':file_path' => $file_path]),
        $this->t('This file has %lines_number lines.', ['%lines_number' => $lines_number]),
      ];

      for ($line = $start_line; $line >= $end_line; --$line) {
        // Parse the line $line.
        $file->seek($line);
        $parsed_line = [];
        if (\is_string($file->current()) && !empty($file->current())) {
          $parsed_line = $this->lineParser->parse($file->current());
        }

        if (\is_array($parsed_line) && !empty($parsed_line)) {
          // It could be weird for a user to deal with a line number of 0
          // so we increment it.
          $human_readable_number_line = $file->key() + 1;
          // Format message: truncate and add link to see detail page.
          $message = $this->lineFormatter->formatData(SyslogFormatEntry::MESSAGE, $parsed_line);
          if (\is_string($message) && !empty($message)) {
            $message = $this->lineFormatter->formatLinkedMessage($message, $human_readable_number_line);
          }

          // Add line information as a row.
          $rows[] = [
            'data' => [
              // Cells.
              $human_readable_number_line,
              $this->lineFormatter->formatData(SyslogFormatEntry::TYPE, $parsed_line),
              $this->lineFormatter->formatData(SyslogFormatEntry::TIMESTAMP, $parsed_line),
              $message,
              $this->lineFormatter->formatData(SyslogFormatEntry::USER, $parsed_line),
            ],
          ];
        }
      }
      // Closing the file handle.
      $file = NULL;
    }
    else {
      $this->messenger()
        ->addError($this->t("You can't read the log file :file_path. The file has to be readable by the Apache user.", [':file_path' => $file_path]));
    }

    $this->buildInformationMessage($build, $information_message);

    $build['syslog_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'id' => 'admin-syslog',
        'class' => [
          'admin-syslog',
        ],
      ],
      '#empty' => $this->t('No log messages available.'),
    ];
    $build['syslog_pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Construct a header similar as the dblog module one.
   *
   * @return array
   *   The header for the overview table.
   */
  protected function constructOverviewTableHeader() {
    $syslog_available_format = SyslogFormatEntry::getFormats();
    return [
      [
        'data' => $this->t('Line'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      [
        'data' => $syslog_available_format[SyslogFormatEntry::TYPE],
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $syslog_available_format[SyslogFormatEntry::TIMESTAMP],
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      [
        'data' => $syslog_available_format[SyslogFormatEntry::MESSAGE],
      ],
      [
        'data' => $syslog_available_format[SyslogFormatEntry::USER],
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ];
  }

  /**
   * Displays details about a specific syslog log message.
   *
   * @param int $line_number
   *   The line number of the log message.
   *
   * @return array
   *   If the line is located in the syslog file and is correctly formatted,
   *   a build array in the format expected by drupal_render();
   */
  public function lineDetails($line_number) {
    // This number corresponds to the human readable one
    // so we need to decrement it to deal with the line number
    // used for SplFileObject.
    --$line_number;
    // Initialize variables.
    $rows = [];
    $build = [];
    $information_message = [];

    // Syslog module have to use a specific file for the Drupal log.
    $file_path = $this->config('syslog_watcher.settings')
      ->get('syslog_file_path');

    if (empty($file_path)) {
      $syslog_logging_settings_path = new Url('system.logging_settings');
      $this->messenger()
        ->addError($this->t('No file path has been found. <a href=":url">Check the configuration</a>.', [':config' => $syslog_logging_settings_path->toString()]));
      return $build;
    }

    $syslog_format_array = SyslogFormatEntry::getFormats();

    if (\is_string($file_path) && \is_readable($file_path)) {
      $information_message = [
        $this->t('You are reading the line :line_number of the log file :file_path.', [
          ':line_number' => ($line_number + 1),
          ':file_path' => $file_path,
        ]),
      ];

      $file = new \SplFileObject($file_path);

      // Go to line $line_number.
      $file->seek($line_number);

      // Parse the line $line_number.
      $parsed_line = '';
      if (\is_string($file->current()) && !empty($file->current())) {
        $parsed_line = $this->lineParser->parse($file->current());
      }

      // Add line information as a row.
      if (\is_array($parsed_line) && !empty($parsed_line)) {
        foreach ($syslog_format_array as $syslog_format => $syslog_label) {
          if (isset($parsed_line[$syslog_format])) {
            $rows[] = [
              [
                'data' => $syslog_label,
                'header' => TRUE,
              ],
              [
                'data' => $this->lineFormatter->formatData($syslog_format, $parsed_line),
              ],
            ];
          }
        }
      }
      $build['syslog_details_raw_line'] = [
        '#type' => 'details',
        '#title' => $this->t('Raw Line'),
      ];
      $build['syslog_details_raw_line']['raw_line'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $file->current(),
      ];
      // Closing the file handle.
      $file = NULL;
    }
    else {
      $this->messenger()
        ->addError($this->t("You can't read the log file :file_path. The file has to be readable by the Apache user.", [':file_path' => $file_path]));
    }

    $this->buildInformationMessage($build, $information_message);

    $build['syslog_table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#attributes' => [
        'id' => 'admin-syslog',
        'class' => [
          'admin-syslog',
        ],
      ],
      '#empty' => $this->t('No log messages available.'),
    ];

    return $build;
  }

  /**
   * Build the renderable array containing informative messages.
   *
   * @param array $build
   *   The build renderable array.
   * @param array $messages
   *   An array containing the messages.
   */
  protected function buildInformationMessage(array &$build, array $messages): void {
    $build['information_message'] = [
      '#type' => 'container',
      '#weight' => (int) -10,
    ];
    foreach ($messages as $message) {
      $build['information_message'][] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $message,
      ];
    }
  }

}
