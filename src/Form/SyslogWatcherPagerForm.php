<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\syslog_watcher\Services\PagerHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the syslog logging pager form.
 */
class SyslogWatcherPagerForm extends FormBase {

  /**
   * Default value for the number of lines per page.
   */
  public const LINES_PER_PAGE_DEFAULT_VALUE = 50;

  /**
   * Default options for the number of lines per page.
   */
  public const LINES_PER_PAGE_DEFAULT_OPTIONS = [
    5,
    10,
    25,
    50,
  ];

  /**
   * The pager helper.
   *
   * @var \Drupal\syslog_watcher\Services\PagerHelperInterface
   */
  protected $pagerHelper;

  /**
   * Constructs a new SyslogWatcherPagerForm object.
   *
   * @param \Drupal\syslog_watcher\Services\PagerHelperInterface $pager_helper
   *   The pager helper.
   */
  public function __construct(PagerHelperInterface $pager_helper) {
    $this->pagerHelper = $pager_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('syslog_watcher.pager_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'syslog_watcher_pager_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['pager'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager'),
      '#open' => FALSE,
    ];

    /** @var array $options */
    $options = $this->configFactory()->get('syslog_watcher.settings')
      ->get('lines_per_page.default_options');
    $form['pager']['lines_per_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of lines per page to display'),
      '#options' => \array_combine($options, $options),
      '#default_value' => $this->pagerHelper->getLinesPerPage(),
    ];

    $form['pager']['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'container-inline',
        ],
      ],
    ];
    $form['pager']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      // Prevent op from showing up in the query string.
      '#name' => '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var string $route */
    $route = $this->getRouteMatch()->getRouteName();
    $input_key = 'lines_per_page';
    if (empty($form_state->getValue($input_key))) {
      // Redirect to the page with empty value to use the default one.
      $page_path = Url::fromRoute($route);
    }
    else {
      // Redirect to the page with lines per page as GET parameter.
      $user_input = $form_state->getValue($input_key);
      $page_path = Url::fromRoute($route, [$input_key => $user_input]);
    }

    $form_state->setRedirectUrl($page_path);
  }

}
