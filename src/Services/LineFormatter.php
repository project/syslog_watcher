<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\syslog_watcher\SyslogFormatEntry;

/**
 * Provides a service to format the line from a syslog file.
 */
class LineFormatter implements LineFormatterInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs a LineFormatter object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   */
  public function __construct(DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_manager) {
    $this->dateFormatter = $date_formatter;
    $this->userStorage = $entity_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function formatData($syslog_format, array $parsed_line) {
    $data = NULL;
    if (isset($parsed_line[$syslog_format])) {
      switch ($syslog_format) {
        case SyslogFormatEntry::MESSAGE:
          $data = Xss::filterAdmin($parsed_line[$syslog_format]);
          break;

        case SyslogFormatEntry::TIMESTAMP:
          $data = $this->dateFormatter->format($parsed_line[$syslog_format], 'short');
          break;

        case SyslogFormatEntry::USER:
          $username = [
            '#theme' => 'username',
            '#account' => $this->userStorage->load($parsed_line[$syslog_format]),
          ];
          $data = ['data' => $username];
          break;

        default:
          $data = $parsed_line[$syslog_format];
          break;
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function formatLinkedMessage($message, $line_number = NULL) {
    $message_title = Unicode::truncate(Html::decodeEntities(\strip_tags($message)), self::TITLE_MAX_LENGTH, TRUE, TRUE);
    $message_log_text = Unicode::truncate($message_title, self::TEXT_MAX_LENGTH, TRUE, TRUE);
    // The link generator will escape any unsafe HTML entities in the
    // final text.
    return new Link($message_log_text, new Url('syslog_watcher.line', ['line_number' => $line_number], [
      'attributes' => [
        // Provide a title for the link for useful hover hints. The
        // Attribute object will escape any unsafe HTML entities in the
        // final text.
        'title' => $message_title,
      ],
    ]));
  }

}
