<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

/**
 * Provides an interface defining a line formatter.
 */
interface LineFormatterInterface {

  /**
   * Title max length.
   */
  public const TITLE_MAX_LENGTH = 256;

  /**
   * Text max length.
   */
  public const TEXT_MAX_LENGTH = 56;

  /**
   * Format a data according to its format entry.
   *
   * @param string $syslog_format
   *   The entry you want to format.
   * @param array $parsed_line
   *   An array resulted from the function SyslogWatcherController::parseLine().
   *
   * @return array|null|string
   *   The formatted data.
   */
  public function formatData($syslog_format, array $parsed_line);

  /**
   * Format a message to have to it linked to the line detail page.
   *
   * @param string $message
   *   The message to format.
   * @param int $line_number
   *   The line number of the message in order to redirect to the detail page.
   *
   * @return \Drupal\Core\Link|string
   *   The formatted message.
   */
  public function formatLinkedMessage($message, $line_number = NULL);

}
