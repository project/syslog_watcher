<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\syslog_watcher\SyslogFormatEntry;

/**
 * Provides a service to parse the line from a syslog file.
 */
class LineParser implements LineParserInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a LineParser object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($line) {
    // We assume every log entry starts with "M j H:i:s hostname identity: "
    // so we remove this part before exploding the remainder.
    $fake_beginning = \date('M j H:i:s') . ' ' . \gethostname() . ' drupal: ';
    $fake_beginning_length = \strlen($fake_beginning);
    $match = \substr($line, 0, $fake_beginning_length);
    /** @var string $new_line */
    $new_line = \str_replace($match, '', $line);

    // Explode the line to work with it latter.
    /** @var non-empty-string $syslog_separator */
    $syslog_separator = $this->configFactory->get('syslog_watcher.settings')
      ->get('separator');
    $exploded_line = \explode($syslog_separator, $new_line);

    /** @var string $syslog_format */
    // Create an array with format defined by the configuration form.
    $syslog_format = $this->configFactory->get('syslog.settings')->get('format');
    /** @var array $syslog_format_array */
    $syslog_format_array = \explode($syslog_separator, $syslog_format);

    if (\count($syslog_format_array) == \count($exploded_line)) {
      // Replace keys with format label.
      $result = \array_combine($syslog_format_array, $exploded_line);
    }
    else {
      // There is an issue with the line, display the truncated raw line.
      $result = $syslog_format_array;
      $result[SyslogFormatEntry::MESSAGE] = Unicode::truncate(Html::decodeEntities(\strip_tags(Xss::filterAdmin($line))), self::LINE_MAX_LENGTH, TRUE, TRUE);
    }

    return $result;
  }

}
