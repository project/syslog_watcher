<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

/**
 * Provides an interface defining a line parser.
 */
interface LineParserInterface {

  /**
   * Line max length.
   */
  public const LINE_MAX_LENGTH = 256;

  /**
   * Parse a line.
   *
   * @param string $line
   *   The line to parse.
   *
   * @return mixed
   *   An array where keys are internal tokens and values the corresponding
   *   values extracted from the log file.
   */
  public function parse($line);

}
