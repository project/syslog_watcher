<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides pager helper methods.
 */
class PagerHelper implements PagerHelperInterface {

  use StringTranslationTrait;

  /**
   * The lines per page query parameter.
   */
  public const LINES_PER_PAGE_QUERY_PARAMETER = 'lines_per_page';

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Constructs a PagerHelper object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   */
  public function __construct(
    RequestStack $request_stack,
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    PagerManagerInterface $pager_manager
  ) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->pagerManager = $pager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function initPager($lines_number, $lines_per_page = NULL) {
    // Initialize the number of lines per page if necessary.
    if (!isset($lines_per_page)) {
      $lines_per_page = $this->getLinesPerPage();
    }

    $start_line = NULL;
    $end_line = NULL;
    // The number of lines per page could be null if an error occurred.
    if (isset($lines_per_page)) {
      $pager = $this->pagerManager->createPager($lines_number, $lines_per_page, 0);
      $page = $pager->getCurrentPage();
      $offset = $lines_per_page * $page;
      $max_line = ($lines_per_page + $offset <= $lines_number) ? ($lines_per_page + $offset) : $lines_number;
      $start_line = $lines_number - $offset - 1;
      $end_line = $lines_number - $max_line;
    }

    return [$start_line, $end_line];
  }

  /**
   * {@inheritdoc}
   */
  public function getLinesPerPage() {
    $config = $this->configFactory->get('syslog_watcher.settings');

    $current_request = $this->requestStack->getCurrentRequest();
    if ($current_request) {
      $query = $current_request->query;
    }

    // Retrieve the number of lines per page from the request.
    if (isset($query) && $query->has(static::LINES_PER_PAGE_QUERY_PARAMETER)) {
      // Convert the parameter to an integer value to check if it is valid.
      /** @var string $lines_per_page_parameter */
      $lines_per_page_parameter = $query->get(static::LINES_PER_PAGE_QUERY_PARAMETER);
      $lines_per_page = (int) ($lines_per_page_parameter);
      // The parameter has to be one of the default options.
      // This avoid users to messing around with the parameter
      // (try to display too many lines for instance etc..).
      /** @var array $defaultLinesPerPage */
      $defaultLinesPerPage = $config->get('lines_per_page.default_options');
      if (!\in_array($lines_per_page, $defaultLinesPerPage, TRUE)) {
        $this->messenger
          ->addError($this->t('The @param parameter is not valid.', ['@param' => static::LINES_PER_PAGE_QUERY_PARAMETER]));
        /** @var int|null $lines_per_page */
        $lines_per_page = $config->get('lines_per_page.default_value');
      }
    }
    // The request do not define the number of lines per page
    // so use the default value.
    else {
      /** @var int|null $lines_per_page */
      $lines_per_page = $config->get('lines_per_page.default_value');
    }

    return $lines_per_page;
  }

}
