<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher\Services;

/**
 * Provides pager helper methods.
 */
interface PagerHelperInterface {

  /**
   * Initialize the pager.
   *
   * @param int $lines_number
   *   The total number of lines to be paged.
   * @param int $lines_per_page
   *   The number of lines the calling code will display per page.
   *
   * @return array
   *   An array containing the number of the line to start and to end.
   */
  public function initPager($lines_number, $lines_per_page = NULL);

  /**
   * Get number of lines per page to display.
   *
   * @return int|null
   *   The number of lines per page to display if success. NULL otherwise.
   */
  public function getLinesPerPage();

}
