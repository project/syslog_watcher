<?php

declare(strict_types = 1);

namespace Drupal\syslog_watcher;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines various format entries syslog could use.
 */
class SyslogFormatEntry {

  /**
   * Syslog Format Entry -- the base url format.
   */
  public const BASE_URL = '!base_url';

  /**
   * Syslog Format Entry -- the timestamp format.
   */
  public const TIMESTAMP = '!timestamp';

  /**
   * Syslog Format Entry -- the type format.
   */
  public const TYPE = '!type';

  /**
   * Syslog Format Entry -- the ip format.
   */
  public const IP = '!ip';

  /**
   * Syslog Format Entry -- the request uri format.
   */
  public const REQUEST_URI = '!request_uri';

  /**
   * Syslog Format Entry -- the referer format.
   */
  public const REFERER = '!referer';

  /**
   * Syslog Format Entry -- the uid format.
   */
  public const USER = '!uid';

  /**
   * Syslog Format Entry -- the link format.
   */
  public const LINK = '!link';

  /**
   * Syslog Format Entry -- the message format.
   */
  public const MESSAGE = '!message';

  /**
   * An array with the syslog format entries as keys and labels as values.
   *
   * @var array
   */
  protected static $formats;

  /**
   * Returns a list of syslog format entries.
   *
   * @return array
   *   Array of the possible syslog format entries.
   */
  public static function getFormats() {
    if (!static::$formats) {
      static::$formats = [
        static::BASE_URL => new TranslatableMarkup('Base Url'),
        static::TIMESTAMP => new TranslatableMarkup('Date'),
        static::TYPE => new TranslatableMarkup('Type'),
        static::IP => new TranslatableMarkup('IP address'),
        static::REQUEST_URI => new TranslatableMarkup('Request URI'),
        static::REFERER => new TranslatableMarkup('Referer'),
        static::USER => new TranslatableMarkup('User'),
        static::LINK => new TranslatableMarkup('Link'),
        static::MESSAGE => new TranslatableMarkup('Message'),
      ];
    }

    return static::$formats;
  }

}
